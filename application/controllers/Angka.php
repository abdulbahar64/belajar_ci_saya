<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Angka extends CI_Controller {


	public function angkaGanjil()
	{
		$this->load->view('angkaganjil');
	}
	
	public function angkaGenap()
	{
		$this->load->view('angkagenap');
	}
}
